import React from "react";
import "./homePage.scss";
import Directory from "../../components/directory/Directory";

export default function HomePage() {
  return (
    <div className="homepage">
      <Directory></Directory>
    </div>
  );
}
