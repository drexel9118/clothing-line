import React, { Component } from "react";
import "./style.scss";
import FormInput from "../form-input/FormInput";
import Button from "../custom-button/Button";
import { signinWithGoogle } from "../../firebase/config";

export default class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ email: "", password: "" });
  };

  handleChange = event => {
    const { value, name } = event.target;
    this.setState({ [name]: value });
  };
  render() {
    return (
      <div className="sign-in">
        <h2>I have already have an accaount</h2>
        <span>Sign in with your email and password</span>
        <form onSubmit={this.handleSubmit}>
          <FormInput
            name="email"
            value={this.state.email}
            handleChange={this.handleChange}
            label="email"
            required
          />

          <FormInput
            name="password"
            type="password"
            value={this.state.password}
            handleChange={this.handleChange}
            label="password"
            required
          />
          <div className="buttons">
            <Button type="submit">Sign-in</Button>
            <Button onClick={signinWithGoogle} google>
              Sign-in with google
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
