import React from "react";
import "./style.scss";

export default function Button({ children, google, ...props }) {
  return (
    <button className={`${google ? "google" : ""} button`} {...props}>
      {children}
    </button>
  );
}
