import React from "react";
import ItemCollection from "../item-collection/ItemCollection";
import "./style.scss";

export default function PreviewCollection({ title, items }) {
  return (
    <div className="preview-collection">
      <h1 className="title">{title.toUpperCase()}</h1>
      <div className="preview">
        {items
          .filter((item, idex) => idex < 4)
          .map(({ id, ...itemProps }) => (
            <ItemCollection key={id} {...itemProps} />
          ))}
      </div>
    </div>
  );
}
