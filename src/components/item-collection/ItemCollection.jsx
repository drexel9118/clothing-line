import React from "react";
import "./style.scss";

export default function ItemCollection({ id, name, price, imageUrl }) {
  return (
    <div className="item-collection">
      <div
        className="image"
        style={{ backgroundImage: `url(${imageUrl})` }}
      ></div>
      <div className="collection-footer">
        <div className="name">{name}</div>
        <div className="price">{price}</div>
      </div>
    </div>
  );
}
