import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyD72hMM52fV2cMdQ1OYZvb2K6DNYNXLvrs",
  authDomain: "clothing-a772a.firebaseapp.com",
  databaseURL: "https://clothing-a772a.firebaseio.com",
  projectId: "clothing-a772a",
  storageBucket: "clothing-a772a.appspot.com",
  messagingSenderId: "352266160464",
  appId: "1:352266160464:web:e1572387ac7405dad88455"
};

export const createUser = async (authUser, otherData) => {
  if (!authUser) return;

  const userRef = await firestore.doc(`users/${authUser.uid}`).get();
  if (!userRef.exists) {
    const { displayName, email } = authUser;
    const createdAt = new Date().toISOString();

    try {
      firestore.doc(`users/${authUser.uid}`).set({
        displayName,
        email,
        createdAt,
        ...otherData
      });
    } catch (error) {
      console.error("error", error.message);
    }
  }

  return firestore.doc(`users/${authUser.uid}`);
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });
export const signinWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
